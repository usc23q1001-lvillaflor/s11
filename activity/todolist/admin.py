from django.contrib import admin

# Register your models here.
from .models import ToDoItem, Events

admin.site.register(ToDoItem)
admin.site.register(Events)